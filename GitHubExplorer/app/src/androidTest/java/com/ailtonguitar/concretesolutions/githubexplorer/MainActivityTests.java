package com.ailtonguitar.concretesolutions.githubexplorer;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.w3c.dom.Text;

import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ailtonguitar.concretesolutions.githubexplorer.adapters.GitHubRepoListAdapter_;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoItem;
import com.robotium.solo.Condition;
import com.robotium.solo.Solo;

/**
 * Created by Ailton on 05/01/2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTests {

    @Rule
    public ActivityTestRule<MainActivity_> activityTestRule =
            new ActivityTestRule<>(MainActivity_.class);

    private Solo solo;

    @Test
    public void sholdBePossibleToLoadRepositoriesWithPaging() throws Exception {
        solo.assertCurrentActivity("The current activity is not MainActivity", MainActivity_.class);

        SwipeRefreshLayout swipe = (SwipeRefreshLayout) solo.getView(R.id.swipe);

        solo.waitForCondition(new Condition() {
            @Override
            public boolean isSatisfied() {
                return !swipe.isRefreshing();
            }
        }, 10000);

        RecyclerView recyclerView = (RecyclerView) solo.getView(R.id.recyclerview);
        GitHubRepoListAdapter_ adapter = (GitHubRepoListAdapter_)recyclerView.getAdapter();

        Assert.assertEquals(30, adapter.getItemCount());
    }

    @Test
    public void sholdBePossibleToRenderRepositoryCard() throws Exception {
        solo.assertCurrentActivity("The current activity is not MainActivity", MainActivity_.class);
        SwipeRefreshLayout swipe = (SwipeRefreshLayout) solo.getView(R.id.swipe);

        solo.waitForCondition(new Condition() {
            @Override
            public boolean isSatisfied() {
                return !swipe.isRefreshing();
            }
        }, 10000);

        RecyclerView recyclerView = (RecyclerView) solo.getView(R.id.recyclerview);
        CardView card = (CardView) recyclerView.getChildAt(0);
        GitHubRepoListAdapter_ adapter = (GitHubRepoListAdapter_) recyclerView.getAdapter();
        GitHubRepoItem item = adapter.getItem(0);

        TextView title = (TextView)card.findViewById(R.id.repositorytitle);
        Assert.assertEquals(item.getFullName(), title.getText());

        TextView description = (TextView)card.findViewById(R.id.repositorydescription);
        Assert.assertTrue(description.getText().length() <= 80);

        TextView userName = (TextView)card.findViewById(R.id.username);
        Assert.assertEquals(item.getUser().getLogin(), userName.getText());
    }

    @Test
    public void sholdBePossibleToOpenPullRequestsActivityOnClick() throws Exception {
        solo.assertCurrentActivity("The current activity is not MainActivity", MainActivity_.class);
        solo.clickInRecyclerView(0);
        solo.assertCurrentActivity( "was not possible open PullRequestActivity", PullRequestActivity_.class);
        solo.goBack();
    }

    @Before
    public void setUp() throws Exception {
        solo = new Solo(InstrumentationRegistry.getInstrumentation(),
                activityTestRule.getActivity());
    }

    @After
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }
}
