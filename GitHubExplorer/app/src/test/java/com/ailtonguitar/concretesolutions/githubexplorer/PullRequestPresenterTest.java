package com.ailtonguitar.concretesolutions.githubexplorer;

import android.content.Context;
import android.widget.TextView;

import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubPullRequest;
import com.ailtonguitar.concretesolutions.githubexplorer.presenters.PullRequestPresenter;
import com.ailtonguitar.concretesolutions.githubexplorer.views.PullRequestView;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.robolectric.Robolectric;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Ailton on 05/01/2017.
 */
public class PullRequestPresenterTest {
    @Test
    public void shouldBePossibleCalculateItemsByStatus() throws Exception {
        PullRequestView view = (PullRequestView)Mockito.mock(PullRequestView.class);
        PullRequestPresenter presenter = new PullRequestPresenter();

        presenter.onViewAttached(view);

        List<GitHubPullRequest> pulls = new ArrayList<GitHubPullRequest>();

        GitHubPullRequest request = new GitHubPullRequest();
        request.setState("open");
        pulls.add(request);

        request = new GitHubPullRequest();
        request.setState("closed");
        pulls.add(request);

        request = new GitHubPullRequest();
        request.setState("closed");
        pulls.add(request);

        presenter.loadCounts(pulls);

        verify(view, times(1)).showOpenedPullRequestCount(1);
        verify(view, times(1)).showClosedPullRequestCount(2);
    }

    @Test
    public void shouldBePossibleCalculateItemsByStatusWithNullValue() throws Exception {
        PullRequestView view = (PullRequestView)Mockito.mock(PullRequestView.class);
        PullRequestPresenter presenter = new PullRequestPresenter();
        presenter.onViewAttached(view);
        presenter.loadCounts(null);
        verify(view, times(1)).showOpenedPullRequestCount(0);
        verify(view, times(1)).showClosedPullRequestCount(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotBePossibleCalculateItemsByStatusWithNullView() throws Exception {
        PullRequestPresenter presenter = new PullRequestPresenter();
        presenter.onViewAttached(null);
        presenter.loadCounts(new ArrayList<GitHubPullRequest>());
    }
}
