package com.ailtonguitar.concretesolutions.githubexplorer;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ailtonguitar.concretesolutions.githubexplorer.adapters.GitHubRepoListAdapter;
import com.ailtonguitar.concretesolutions.githubexplorer.base.presenters.BasePresenterActivity;
import com.ailtonguitar.concretesolutions.githubexplorer.base.presenters.PresenterFactory;
import com.ailtonguitar.concretesolutions.githubexplorer.listeners.EndlessScrollListener;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoItem;
import com.ailtonguitar.concretesolutions.githubexplorer.presenters.MainPresenter;
import com.ailtonguitar.concretesolutions.githubexplorer.presenters.MainPresenterFactory;
import com.ailtonguitar.concretesolutions.githubexplorer.views.MainView;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends BasePresenterActivity<MainPresenter, MainView> implements MainView {

    public static final int PAGE_SIZE = 30;
    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView emptyview;

    @ViewById
    RecyclerView recyclerview;

    @ViewById
    SwipeRefreshLayout swipe;

    @Bean
    GitHubRepoListAdapter adapter;

    @Override
    protected PresenterFactory<MainPresenter> getPresenterFactory() {
        return new MainPresenterFactory();
    }

    @Override
    protected void onPresenterPrepared() {
        setSupportActionBar(toolbar);

        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setAdapter(adapter);
        recyclerview.addOnScrollListener(new EndlessScrollListener(recyclerview, PAGE_SIZE) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                MainActivity.this.getPresenter().loadRepositories(page);
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MainActivity.this.adapter.clear();
                MainActivity.this.getPresenter().refreshRepositories();
            }
        });

        this.adapter.setOnLoaded(data -> {
            if(data.isEmpty())
                MainActivity.this.getPresenter().refreshRepositories();
        });

        this.initLoader(this.adapter.LOADER_ID, this.adapter);

        this.adapter.setOnItemClick(data -> {
            Intent intent = new Intent(this, PullRequestActivity_.class);
            intent.putExtra("repositoryFullName", data.getFullName());
            intent.putExtra("repository", data.getName());
            intent.putExtra("owner", data.getUser().getLogin());
            intent.putExtra("repositoryId", data.getId());
            startActivity(intent);
        });
    }

    @Override
    public void showRepositories(GitHubRepoItem[] items) {
        emptyview.setVisibility(View.GONE);

        this.adapter.appendItems(items);

        if(this.adapter.isEmpty())
            showEmptyMessage();
    }

    @Override
    public void showLoading(boolean visible) {
        // SwipeRefreshLayout indicator does not appear when the setRefreshing(true) is called
        // before the SwipeRefreshLayout.onMeasure(). Create a runnable to solve this issue.
        swipe.post(new Runnable() {
            @Override
            public void run() {
                swipe.setRefreshing(visible);
            }
        });
    }

    @Override
    public void showNetworkConnectionError() {
        this.showLoading(false);
        showMessage(R.string.error_network_connection_broken);
    }

    @Override
    public void showErrorOnLoad() {
        showMessage(R.string.error_load_data);
    }

    public void showMessage(int message){
        boolean isEmpty = this.adapter.isEmpty();
        if(isEmpty){
            emptyview.setText(getResources().getString(message));
            emptyview.setVisibility(View.VISIBLE);
        }else{
            emptyview.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
        }
    }

    public void showEmptyMessage(){
        showMessage(R.string.no_results);
    }
}
