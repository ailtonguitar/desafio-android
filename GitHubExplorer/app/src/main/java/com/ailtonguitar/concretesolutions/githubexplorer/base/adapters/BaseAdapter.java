package com.ailtonguitar.concretesolutions.githubexplorer.base.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

/**
 * Created by Ailton on 04/01/2017.
 */
public abstract class BaseAdapter<T, H extends BaseViewHolder> extends RecyclerView.Adapter<H>{
    protected List<T> repositories;
    protected Context context;

    public BaseAdapter(Context context) {
        this.context = context;
        this.repositories = new ArrayList<T>();
    }

    public T getItem(int index){
        return this.repositories.get(index);
    }

    public void appendItems(T[] items) {
        for (T item : items)
            this.repositories.add(item);
        this.notifyDataSetChanged();
    }

    public List<T> getItems(){
        return this.repositories;
    }

    public void clear() {
        this.repositories.clear();
        this.notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return this.repositories.isEmpty();
    }

    @Override
    public void onBindViewHolder(H holder, int position) {
        holder.dataBind(this.repositories.get(position));
    }

    @Override
    public int getItemCount() {
        return this.repositories.size();
    }
}
