package com.ailtonguitar.concretesolutions.githubexplorer.services;

import com.ailtonguitar.concretesolutions.githubexplorer.BuildConfig;
import com.ailtonguitar.concretesolutions.githubexplorer.api.GitHubApiClient;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by Ailto on 03/01/2017.
 */
public class GitHubService {

    private  static  final String BASE_URL = "https://api.github.com/";

    private static GitHubApiClient instance;

    private GitHubService(){}

    private static OkHttpClient getHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        // Adding interceptor to log all requests
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
        }

        // Adding header
        httpClient.addInterceptor(chain -> {
            Request request = chain.request()
                    .newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .build();
            return chain.proceed(request);
        });
        return httpClient.build();
    }

    private static GitHubApiClient buildClient(){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build().create(GitHubApiClient.class);
    }

    public static GitHubApiClient getClient(){
        if(instance == null)
            instance = buildClient();
        return instance;
    }
}
