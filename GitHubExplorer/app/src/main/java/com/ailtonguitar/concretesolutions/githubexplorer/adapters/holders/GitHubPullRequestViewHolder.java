package com.ailtonguitar.concretesolutions.githubexplorer.adapters.holders;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.ailtonguitar.concretesolutions.githubexplorer.R;
import com.ailtonguitar.concretesolutions.githubexplorer.base.adapters.BaseViewHolder;
import com.ailtonguitar.concretesolutions.githubexplorer.helpers.DateHelper;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubPullRequest;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoItem;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import rx.functions.Action1;

/**
 * Created by Ailton on 05/01/2017.
 */
public class GitHubPullRequestViewHolder extends BaseViewHolder<GitHubPullRequest> {
    private Action1<GitHubPullRequest> onClick;

    public GitHubPullRequestViewHolder(View itemView, Action1<GitHubPullRequest> onClick) {
        super(itemView);
        this.onClick = onClick;
    }

    @Override
    public void dataBind(final GitHubPullRequest item) {
        TextView title = (TextView) this.itemView.findViewById(R.id.pullrequesttitle);
        title.setText(item.getTitle());

        TextView description = (TextView) this.itemView.findViewById(R.id.pullrequestbody);
        description.setText(item.getBody());

        TextView createdAt = (TextView) this.itemView.findViewById(R.id.createdat);
        createdAt.setText(DateHelper.formatDate(item.getCreatedAt(), "MM/dd/yyyy HH:mm:ss"));

        TextView username = (TextView) this.itemView.findViewById(R.id.username);
        username.setText(item.getUser().getLogin());

        CircleImageView image = (CircleImageView) this.itemView.findViewById(R.id.userpicture);

        Picasso.with(this.itemView.getContext()).load(Uri.parse(item.getUser().getAvatarUrl()))
                //.placeholder(R.drawable.images).error(R.drawable.ic_launcher)
                .into(image);

        this.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.call(item);
            }
        });
    }}
