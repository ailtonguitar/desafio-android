package com.ailtonguitar.concretesolutions.githubexplorer.presenters;

import com.ailtonguitar.concretesolutions.githubexplorer.base.presenters.PresenterFactory;

/**
 * Created by Ailto on 05/01/2017.
 */
public class PullRequestPresenterFactory implements PresenterFactory<PullRequestPresenter> {
    @Override
    public PullRequestPresenter create() {
        return new PullRequestPresenter();
    }
}
