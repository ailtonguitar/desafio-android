package com.ailtonguitar.concretesolutions.githubexplorer.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ailton on 05/01/2017.
 */
public class DateHelper {
    public static String formatDate(Date date, String pattern){
        SimpleDateFormat dt = new SimpleDateFormat(pattern);
        return dt.format(date);
    }
}
