package com.ailtonguitar.concretesolutions.githubexplorer.base.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ailtonguitar.concretesolutions.githubexplorer.R;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoItem;

/**
 * Created by Ailton on 04/01/2017.
 */
public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {
    protected BaseViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void dataBind(T item);
}
