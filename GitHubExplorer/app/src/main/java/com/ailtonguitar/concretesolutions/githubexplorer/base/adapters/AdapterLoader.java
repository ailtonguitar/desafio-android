package com.ailtonguitar.concretesolutions.githubexplorer.base.adapters;

import android.content.Context;
import android.support.v4.content.Loader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ailton on 05/01/2017.
 */
public class AdapterLoader<T> extends Loader<List<T>> {
    private List<T> data;
    public AdapterLoader(Context context) {
        super(context);
    }

    public void setData(List<T> data){
        this.data = data;
    }

    @Override
    protected void onStartLoading() {
        if (data != null) {
            deliverResult(data);
            return;
        }

        forceLoad();
    }

    @Override
    protected void onForceLoad() {
        data = new ArrayList<T>();
        deliverResult(data);
    }

    @Override
    public void deliverResult(List<T> data) {
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
    }

    @Override
    protected void onReset() {
        if (data != null)
            data  = new ArrayList<T>();
    }
}
