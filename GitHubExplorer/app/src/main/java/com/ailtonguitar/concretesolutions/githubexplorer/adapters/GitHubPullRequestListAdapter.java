package com.ailtonguitar.concretesolutions.githubexplorer.adapters;

import android.content.Context;
import android.view.View;

import com.ailtonguitar.concretesolutions.githubexplorer.R;
import com.ailtonguitar.concretesolutions.githubexplorer.adapters.holders.GitHubPullRequestViewHolder;
import com.ailtonguitar.concretesolutions.githubexplorer.adapters.holders.GitHubRepoViewHolder;
import com.ailtonguitar.concretesolutions.githubexplorer.base.adapters.StatefulListAdapter;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubPullRequest;

import org.androidannotations.annotations.EBean;

/**
 * Created by Ailto on 05/01/2017.
 */
@EBean
public class GitHubPullRequestListAdapter extends StatefulListAdapter<GitHubPullRequest, GitHubPullRequestViewHolder> {
    public GitHubPullRequestListAdapter(Context context) {
        super(context);
    }

    @Override
    protected GitHubPullRequestViewHolder getViewHolder(View v) {
        return new GitHubPullRequestViewHolder(v, this.onItemClick);
    }

    @Override
    protected int getTemplate() {
        return R.layout.card_pullrequest;
    }
}
