package com.ailtonguitar.concretesolutions.githubexplorer;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ailtonguitar.concretesolutions.githubexplorer.adapters.GitHubPullRequestListAdapter;
import com.ailtonguitar.concretesolutions.githubexplorer.base.presenters.BasePresenterActivity;
import com.ailtonguitar.concretesolutions.githubexplorer.base.presenters.PresenterFactory;
import com.ailtonguitar.concretesolutions.githubexplorer.listeners.EndlessScrollListener;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubPullRequest;
import com.ailtonguitar.concretesolutions.githubexplorer.presenters.PullRequestPresenter;
import com.ailtonguitar.concretesolutions.githubexplorer.presenters.PullRequestPresenterFactory;
import com.ailtonguitar.concretesolutions.githubexplorer.views.PullRequestView;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Ailton on 05/01/2017.
 */
@EActivity(R.layout.activity_pullrequests)
public class PullRequestActivity extends BasePresenterActivity<PullRequestPresenter, PullRequestView> implements PullRequestView {
    public static final int PAGE_SIZE = 30;

    @Extra("repository")
    String repository;

    @Extra("repositoryFullName")
    String repositoryFullName;

    @Extra("owner")
    String owner;

    @Extra("repositoryId")
    int repositoryId;

    @ViewById
    Toolbar toolbar;

    @ViewById
    TextView emptyview;

    @ViewById
    RecyclerView recyclerview;

    @ViewById
    SwipeRefreshLayout swipe;

    @ViewById
    TextView openedPullRequestCount;

    @ViewById
    TextView closedPullRequestCount;

    @ViewById
    LinearLayout countBar;

    @Bean
    GitHubPullRequestListAdapter adapter;

    @Override
    protected PresenterFactory<PullRequestPresenter> getPresenterFactory() {
        return new PullRequestPresenterFactory();
    }

    @Override
    protected void onPresenterPrepared() {
        toolbar.setTitle(this.repositoryFullName);

        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setAdapter(adapter);
        recyclerview.addOnScrollListener(new EndlessScrollListener(recyclerview, PAGE_SIZE) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                getPresenter().loadPullRequests(page);
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PullRequestActivity.this.adapter.clear();
                PullRequestActivity.this.getPresenter().refreshPullRequests();
            }
        });

        this.adapter.setOnLoaded(data -> {
            if (data.isEmpty())
                PullRequestActivity.this.getPresenter().refreshPullRequests();
            else
                getPresenter().loadCounts(data);
        });

        this.initLoader(this.repositoryId, this.adapter);

        this.adapter.setOnItemClick(data -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.getHtmlUrl()));
            startActivity(browserIntent);
        });
    }

    @Override
    public void showPullRequests(GitHubPullRequest[] items) {
        emptyview.setVisibility(View.GONE);

        this.adapter.appendItems(items);

        if(this.adapter.isEmpty())
            showEmptyMessage();
        else
            getPresenter().loadCounts(this.adapter.getItems());

    }

    @Override
    public void showLoading(boolean visible) {
        // SwipeRefreshLayout indicator does not appear when the setRefreshing(true) is called
        // before the SwipeRefreshLayout.onMeasure(). Create a runnable to solve this issue.
        swipe.post(new Runnable() {
            @Override
            public void run() {
                swipe.setRefreshing(visible);
            }
        });

        countBar.setVisibility(visible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showOpenedPullRequestCount(int count) {
        openedPullRequestCount.setText(String.valueOf(count));
    }

    @Override
    public void showClosedPullRequestCount(int count) {
        closedPullRequestCount.setText(String.valueOf(count));
    }

    @Override
    public void showNetworkConnectionError() {
        this.showLoading(false);
        showMessage(R.string.error_network_connection_broken);
    }

    @Override
    public void showErrorOnLoad() {
        showMessage(R.string.error_load_data);
    }

    public void showMessage(int message){
        boolean isEmpty = this.adapter.isEmpty();
        if(isEmpty){
            emptyview.setText(getResources().getString(message));
            emptyview.setVisibility(View.VISIBLE);
        }else{
            emptyview.setVisibility(View.GONE);
            Toast.makeText(PullRequestActivity.this, message, Toast.LENGTH_LONG).show();
        }
    }

    public void showEmptyMessage(){
        showMessage(R.string.no_results);
    }

    @Override
    public String getRepository() {
        return this.repository;
    }

    @Override
    public String getOwner() {
        return this.owner;
    }
}
