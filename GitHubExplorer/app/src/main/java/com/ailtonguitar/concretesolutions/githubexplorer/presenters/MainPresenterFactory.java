package com.ailtonguitar.concretesolutions.githubexplorer.presenters;

import com.ailtonguitar.concretesolutions.githubexplorer.base.presenters.PresenterFactory;

/**
 * Created by Ailton on 03/01/2017.
 */
public class MainPresenterFactory implements PresenterFactory<MainPresenter> {
    @Override
    public MainPresenter create() {
        return new MainPresenter();
    }
}
