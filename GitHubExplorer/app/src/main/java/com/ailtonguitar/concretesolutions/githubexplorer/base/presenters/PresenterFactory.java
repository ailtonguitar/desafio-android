package com.ailtonguitar.concretesolutions.githubexplorer.base.presenters;

/**
 * Created by Ailton on 03/01/2017.
 */
public interface PresenterFactory<T extends Presenter> {
    T create();
}