package com.ailtonguitar.concretesolutions.githubexplorer.base.presenters;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Ailton on 03/01/2017.
 */
public abstract class BasePresenterActivity<P extends Presenter<V>, V> extends AppCompatActivity {
    private static final int LOADER_ID = 200716;
    public P mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initLoader();
    }

    protected <D> Loader<D> initLoader(int id, LoaderManager.LoaderCallbacks<D> callback){
        return getSupportLoaderManager().initLoader(id, null, callback);
    }

    private void initLoader(){
        this.initLoader(LOADER_ID, new LoaderManager.LoaderCallbacks<P>() {
            @Override
            public final Loader<P> onCreateLoader(int id, Bundle args) {
                return new PresenterLoader<P>(BasePresenterActivity.this, BasePresenterActivity.this.getPresenterFactory());
            }

            @Override
            public final void onLoadFinished(Loader<P> loader, P presenter) {
                BasePresenterActivity.this.mPresenter = presenter;
                mPresenter.onViewAttached((V)BasePresenterActivity.this);
                onPresenterPrepared();
            }

            @Override
            public final void onLoaderReset(Loader<P> loader) {
                BasePresenterActivity.this.mPresenter = null;
                onPresenterDestroyed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.onViewAttached((V)this);
    }

    @Override
    protected void onStop() {
        mPresenter.onViewDetached();
        super.onStop();
    }

    protected P getPresenter(){
        return this.mPresenter;
    }

    protected abstract PresenterFactory<P> getPresenterFactory();
    protected abstract void onPresenterPrepared();
    protected void onPresenterDestroyed(){
    }
}
