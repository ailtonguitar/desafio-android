package com.ailtonguitar.concretesolutions.githubexplorer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ailton on 03/01/2017.
 */
public class GitHubRepoListResult {
    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("items")
    private GitHubRepoItem[] items;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public GitHubRepoItem[] getItems() {
        return items;
    }

    public void setItems(GitHubRepoItem[] items) {
        this.items = items;
    }
}
