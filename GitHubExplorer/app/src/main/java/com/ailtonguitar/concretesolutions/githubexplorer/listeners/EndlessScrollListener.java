package com.ailtonguitar.concretesolutions.githubexplorer.listeners;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;

import com.ailtonguitar.concretesolutions.githubexplorer.MainActivity;

/**
 * Created by Ailton on 04/01/2017.
 */
public abstract class EndlessScrollListener extends OnScrollListener {
    private RecyclerView view;
    private int pageSize;

    public EndlessScrollListener(RecyclerView view, int pageSize){
        this.view = view;
        this.pageSize = pageSize;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (dy > 0) {
            LinearLayoutManager lm = (LinearLayoutManager) this.view.getLayoutManager();
            int visibleItemCount = lm.getChildCount();
            int totalItemCount = this.view.getAdapter().getItemCount();
            int pastVisiblesItems = lm.findFirstVisibleItemPosition();
            int currentPage = totalItemCount / this.pageSize;

            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                this.onLoadMore(++currentPage, totalItemCount);
        }
    }

    public abstract void onLoadMore(int page, int totalItemsCount);

}
