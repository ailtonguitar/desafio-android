package com.ailtonguitar.concretesolutions.githubexplorer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ailtonguitar.concretesolutions.githubexplorer.R;
import com.ailtonguitar.concretesolutions.githubexplorer.adapters.holders.GitHubRepoViewHolder;
import com.ailtonguitar.concretesolutions.githubexplorer.base.adapters.StatefulAdapter;
import com.ailtonguitar.concretesolutions.githubexplorer.base.adapters.StatefulListAdapter;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoItem;

import org.androidannotations.annotations.EBean;

import rx.functions.Action1;

/**
 * Created by Ailton on 03/01/2017.
 */
@EBean
public class GitHubRepoListAdapter extends StatefulListAdapter<GitHubRepoItem, GitHubRepoViewHolder> {
    public final int LOADER_ID = 3001;

    public GitHubRepoListAdapter(Context context) {
        super(context);
    }

    @Override
    protected GitHubRepoViewHolder getViewHolder(View v) {
        return new GitHubRepoViewHolder(v, this.onItemClick);
    }

    @Override
    protected int getTemplate() {
        return R.layout.card_repository;
    }
}
