package com.ailtonguitar.concretesolutions.githubexplorer.base.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ailtonguitar.concretesolutions.githubexplorer.R;
import com.ailtonguitar.concretesolutions.githubexplorer.adapters.holders.GitHubRepoViewHolder;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoItem;

import rx.functions.Action1;

/**
 * Created by Ailto on 05/01/2017.
 */
public abstract class StatefulListAdapter<T, H extends BaseViewHolder<T>>  extends StatefulAdapter<T, H> {
        protected Action1<T> onItemClick;

        public StatefulListAdapter(Context context) {
            super(context);
        }

        public void setOnItemClick(Action1<T> onItemClick){
            this.onItemClick = onItemClick;
        }

        protected abstract H getViewHolder(View v);
        protected abstract int getTemplate();

        @Override
        public H onCreateViewHolder(ViewGroup parent, int viewType) {
            final View v = LayoutInflater.from(parent.getContext())
                    .inflate(this.getTemplate(), parent, false);
            return this.getViewHolder(v);
        }
    }
