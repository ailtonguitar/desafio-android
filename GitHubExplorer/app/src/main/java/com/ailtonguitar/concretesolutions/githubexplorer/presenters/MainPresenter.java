package com.ailtonguitar.concretesolutions.githubexplorer.presenters;

import com.ailtonguitar.concretesolutions.githubexplorer.base.presenters.Presenter;
import com.ailtonguitar.concretesolutions.githubexplorer.helpers.NetworkHelper;
import com.ailtonguitar.concretesolutions.githubexplorer.services.GitHubService;
import com.ailtonguitar.concretesolutions.githubexplorer.views.MainView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Ailton on 03/01/2017.
 */
public class MainPresenter implements Presenter<MainView> {

    private MainView view;
    private Subscription subscription;

    @Override
    public void onViewAttached(MainView view) {
        this.view = view;
    }

    public void refreshRepositories(){
        loadRepositories(1);
    }

    public void loadRepositories(int page){
        if(isLoadSubscribed())
            subscription.unsubscribe();

        if(!NetworkHelper.isNetworkAvailable(this.view.getApplicationContext())) {
            this.view.showNetworkConnectionError();
            return;
        }

        subscription = GitHubService.getClient().getPopularRepositories(page)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> {
                    view.showLoading(true);
                }).doOnUnsubscribe(() -> {
                    view.showLoading(false);
                }).subscribe(result -> {
                    this.view.showRepositories(result.getItems());
                }, e -> {
                    this.view.showErrorOnLoad();
                });
    }

    private boolean isLoadSubscribed(){
        return subscription != null && !subscription.isUnsubscribed();
    }

    @Override
    public void onViewDetached() {
        this.view = null;
    }

    @Override
    public void onDestroyed() {

    }
}
