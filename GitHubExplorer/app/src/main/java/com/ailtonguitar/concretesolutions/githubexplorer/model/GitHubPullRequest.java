package com.ailtonguitar.concretesolutions.githubexplorer.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Ailton on 05/01/2017.
 */
public class GitHubPullRequest {
    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("state")
    private String state;

    @SerializedName("body")
    private String body;

    @SerializedName("created_at")
    private Date createdAt;

    @SerializedName("user")
    private GitHubUser user;

    public GitHubUser getUser() {
        return user;
    }

    public void setUser(GitHubUser user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
