package com.ailtonguitar.concretesolutions.githubexplorer.base.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

/**
 * Created by Ailto on 05/01/2017.
 */
public abstract class StatefulAdapter<T, H extends BaseViewHolder> extends BaseAdapter<T, H> implements LoaderManager.LoaderCallbacks<List<T>> {

    private AdapterLoader<T> loader;
    private Action1<List<T>> onLoaded;

    public StatefulAdapter(Context context) {
        super(context);
    }

    public void setOnLoaded(Action1<List<T>> onLoaded){
        this.onLoaded = onLoaded;
    }

    @Override
    public void appendItems(T[] items) {
        super.appendItems(items);
        if(loader != null)
            loader.setData(this.repositories);
    }

    @Override
    public void clear() {
        super.clear();
        if(loader != null)
            loader.setData(this.repositories);
    }

    @Override
    public final Loader<List<T>> onCreateLoader(int id, Bundle args) {
        this.loader= new AdapterLoader<T>(this.context);
        return loader;
    }

    @Override
    public final void onLoadFinished(Loader<List<T>> loader, List<T> data) {
        this.repositories = data;
        if(this.onLoaded != null)
            this.onLoaded.call(this.repositories);
    }

    @Override
    public final void onLoaderReset(Loader<List<T>> loader) {
        this.repositories = new ArrayList<T>();
    }
}
