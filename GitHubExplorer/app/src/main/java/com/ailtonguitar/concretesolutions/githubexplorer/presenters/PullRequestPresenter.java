package com.ailtonguitar.concretesolutions.githubexplorer.presenters;

import com.ailtonguitar.concretesolutions.githubexplorer.base.presenters.Presenter;
import com.ailtonguitar.concretesolutions.githubexplorer.helpers.NetworkHelper;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubPullRequest;
import com.ailtonguitar.concretesolutions.githubexplorer.services.GitHubService;
import com.ailtonguitar.concretesolutions.githubexplorer.views.MainView;
import com.ailtonguitar.concretesolutions.githubexplorer.views.PullRequestView;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Ailton on 05/01/2017.
 */
public class PullRequestPresenter implements Presenter<PullRequestView> {
    private PullRequestView view;
    private Subscription subscription;

    @Override
    public void onViewAttached(PullRequestView view) {
        this.view = view;
    }

    public void refreshPullRequests(){
        loadPullRequests(1);
    }

    public void loadCounts(List<GitHubPullRequest> items){
        if(view == null)
            throw  new IllegalArgumentException("View cannot be null.");

        if(items == null)
        {
            this.view.showOpenedPullRequestCount(0);
            this.view.showClosedPullRequestCount(0);
            return;
        }

        Observable.from(items).filter(x->x.getState().equals("open")).count().subscribe(result -> {
            this.view.showOpenedPullRequestCount(result);
        });

        Observable.from(items).filter(x->x.getState().equals("closed")).count().subscribe(result -> {
            this.view.showClosedPullRequestCount(result);
        });
    }

    public void loadPullRequests(int page){
        if(isLoadSubscribed())
            subscription.unsubscribe();

        if(!NetworkHelper.isNetworkAvailable(this.view.getApplicationContext())) {
            this.view.showNetworkConnectionError();
            return;
        }

        subscription = GitHubService.getClient().getRepositoryPullRequests(view.getRepository(), view.getOwner(), page)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> {
                    view.showLoading(true);
                }).doOnUnsubscribe(() -> {
                    view.showLoading(false);
                }).subscribe(result -> {
                    this.view.showPullRequests(result);
                }, e -> {
                    this.view.showErrorOnLoad();
                    this.view.showOpenedPullRequestCount(0);
                    this.view.showClosedPullRequestCount(0);
                });
    }

    private boolean isLoadSubscribed(){
        return subscription != null && !subscription.isUnsubscribed();
    }

    @Override
    public void onViewDetached() {
        this.view = null;
    }

    @Override
    public void onDestroyed() {

    }
}
