package com.ailtonguitar.concretesolutions.githubexplorer.adapters.holders;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.ailtonguitar.concretesolutions.githubexplorer.R;
import com.ailtonguitar.concretesolutions.githubexplorer.base.adapters.BaseViewHolder;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoItem;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import rx.functions.Action1;

/**
 * Created by Ailton on 03/01/2017.
 */
public class GitHubRepoViewHolder extends BaseViewHolder<GitHubRepoItem> {

    private Action1<GitHubRepoItem> onClick;

    public GitHubRepoViewHolder(View itemView, Action1<GitHubRepoItem> onClick) {
        super(itemView);
        this.onClick = onClick;
    }

    @Override
    public void dataBind(final GitHubRepoItem item) {
        TextView title = (TextView) this.itemView.findViewById(R.id.repositorytitle);
        title.setText(item.getFullName());

        String repoDescription = item.getDescription();

        if (repoDescription != null && repoDescription.length() > 80)
            repoDescription = repoDescription.substring(0, 77) + "...";

        TextView description = (TextView) this.itemView.findViewById(R.id.repositorydescription);
        description.setText(repoDescription);

        TextView username = (TextView) this.itemView.findViewById(R.id.username);
        username.setText(item.getUser().getLogin());

        TextView forks = (TextView) this.itemView.findViewById(R.id.repositoryforks);
        forks.setText(String.valueOf(item.getForksCount()));

        TextView stars = (TextView) this.itemView.findViewById(R.id.repositorystars);
        stars.setText(String.valueOf(item.getStargazersCount()));

        CircleImageView image = (CircleImageView) this.itemView.findViewById(R.id.userpicture);
        Picasso.with(this.itemView.getContext()).load(Uri.parse(item.getUser().getAvatarUrl()))
                //.placeholder(R.drawable.images).error(R.drawable.ic_launcher)
                .into(image);

        this.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.call(item);
            }
        });
    }
}
