package com.ailtonguitar.concretesolutions.githubexplorer.views;

import android.content.Context;

import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoItem;

/**
 * Created by Ailton on 03/01/2017.
 */
public interface MainView {
    void showRepositories(GitHubRepoItem[] items);
    void showLoading(boolean visible);
    Context getApplicationContext();
    void showNetworkConnectionError();
    void showErrorOnLoad();
}
