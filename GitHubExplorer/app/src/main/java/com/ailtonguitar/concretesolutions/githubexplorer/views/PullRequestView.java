package com.ailtonguitar.concretesolutions.githubexplorer.views;

import android.content.Context;

import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubPullRequest;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoItem;

/**
 * Created by Ailto on 05/01/2017.
 */
public interface PullRequestView {
    void showPullRequests(GitHubPullRequest[] items);
    void showLoading(boolean visible);
    void showOpenedPullRequestCount(int count);
    void showClosedPullRequestCount(int count);
    Context getApplicationContext();
    void showNetworkConnectionError();
    void showErrorOnLoad();
    String getRepository();
    String getOwner();
}
