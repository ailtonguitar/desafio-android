package com.ailtonguitar.concretesolutions.githubexplorer.api;

import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubPullRequest;
import com.ailtonguitar.concretesolutions.githubexplorer.model.GitHubRepoListResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Ailton on 03/01/2017.
 */
public interface GitHubApiClient {
    @GET("search/repositories?q=language:Java&sort=stars")
    Observable<GitHubRepoListResult> getPopularRepositories(@Query("page") int page);

    @GET("repos/{user}/{repository}/pulls?state=all")
    Observable<GitHubPullRequest[]> getRepositoryPullRequests(@Path("repository") String repository, @Path("user") String user, @Query("page") int page);
}
